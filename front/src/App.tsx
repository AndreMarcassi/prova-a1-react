import { useState } from 'react';
import AlterarTarefa from './components/AlterarTarefa';
import ListarCategoria from './components/listas/ListarCategoria';
import ListarTarefas from './components/listas/ListarTarefas';
import ListarConcluidas from './components/listas/ListarConcluidas';
import ListarNaoConcluidas from './components/listas/ListarNaoConcluidas';
import CadastrarTarefa from './components/CadastrarTarefa';

type FormType = 'cadastrarTarefa' | 'alterarTarefa' | 'listarCategoria' | 'listartarefas' | 'listarConcluidas' | 'listarNaoConcluidas' | null;


function App() {

    const [activeFormDiv55, setActiveFormDiv55] = useState<FormType | null>('cadastrarTarefa');
    const [activeFormDiv35, setActiveFormDiv35] = useState<FormType | null>('listartarefas');


    const handleButtonClickDiv55 = (form: FormType) => {
        setActiveFormDiv55(form === activeFormDiv55 ? null : form);
    };
      
    const handleButtonClickDiv35 = (form: FormType) => {
        setActiveFormDiv35(form === activeFormDiv35 ? null : form);
    };

    return (

        <div className="flex borda-teste">

            <div className="div-55">

                <div className="flex div-butao">
                    
                    <button className="butao-adm" type="button" onClick={() => handleButtonClickDiv55('cadastrarTarefa')}>Cadastrar Tarefa</button>
                    <button className="butao-adm" type="button" onClick={() => handleButtonClickDiv55('alterarTarefa')}>Alterar Tarefa</button>
                    
                </div>

                <div className="flex div-display" id="display-form">

                    {activeFormDiv55 === 'cadastrarTarefa' && <CadastrarTarefa/>}
                    {activeFormDiv55 === 'alterarTarefa' && <AlterarTarefa/>}

                </div>

                </div>

                <div className="div-35 flex">

                <div className="flex div-butao">

                    <button className="butao-adm" type="button" onClick={() => handleButtonClickDiv35('listarCategoria')}>Listar Categorias</button>

                    <button className="butao-adm" type="button" onClick={() => handleButtonClickDiv35('listartarefas')}>Listar Tarefas</button>
                    <button className="butao-adm" type="button" onClick={() => handleButtonClickDiv35('listarConcluidas')}>Listar Concluidas</button>
                    <button className="butao-adm" type="button" onClick={() => handleButtonClickDiv35('listarNaoConcluidas')}>Listar Não Concluidas</button>

                </div>

                <div className="div-display lista" id="display-list">

                    {activeFormDiv35 === 'listarCategoria' && <ListarCategoria/>}
                    {activeFormDiv35 === 'listartarefas' && <ListarTarefas/>}
                    {activeFormDiv35 === 'listarConcluidas' && <ListarConcluidas/>}
                    {activeFormDiv35 === 'listarNaoConcluidas' && <ListarNaoConcluidas/>}

                </div>

            </div>

        </div>

    );
}

export default App;