import { useEffect, useState } from "react";

function AlterarTarefa(){

    const [id, setId] = useState("");

    function AlterarTarefa(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();

        fetch(`http://localhost:5000/tarefas/alterar/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((resposta) => {
            if (!resposta.ok) {
                throw new Error('Erro ao alterar a tarefa');
            }
            return resposta.json();
        })
        .then((data) => {
            console.log("tarefa atualizado com sucesso:", data);
        })
        .catch((error) => {
            console.error("Erro ao atualizar tarefa:", error);
        });
        

        e.preventDefault();

    }

    return (

        <form className="flex" style={{flexWrap: 'wrap'}} onSubmit={AlterarTarefa}>

            <input className="form-input" type="text" placeholder="Digite o ID da tarefa" onChange={(e) => setId(e.target.value)} required/>

            <input className="form-input" type="submit" value="Confirmar"/>

        </form>

    );

}

export default AlterarTarefa;