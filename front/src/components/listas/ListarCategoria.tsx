import { useEffect, useState } from "react";
import { Categoria } from "../../models/Categoria";

function ListarCategoria() {
    const [categorias, setCategoria] = useState<Categoria[]>([]);

    useEffect(() => {
        carregarCategorias();
    }, []);

    function carregarCategorias() {
        // FETCH ou AXIOS
        fetch("http://localhost:5000/categoria/listar")
            .then((resposta) => resposta.json())
            .then((categorias: Categoria[]) => {
                console.table(categorias);
                setCategoria(categorias);
            })
    }

    return (

        <div>
            
            {categorias.map((categoria) => (

                <ul key={categoria.categoriaId}>

                    <li> ID: {categoria.categoriaId} </li>
                    <li> Nome: {categoria.nome} </li>
                    <br />

                </ul>

            ))}

        </div>

    );

}

export default ListarCategoria;
