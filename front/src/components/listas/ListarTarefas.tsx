import { useEffect, useState } from "react";
import { Tarefa } from "../../models/Tarefa";

function ListarTarefas() {
    const [tarefas, setTarefa] = useState<Tarefa[]>([]);

    useEffect(() => {
        carregarTarefas();
    }, []);

    function carregarTarefas() {
        // FETCH ou AXIOS
        fetch("http://localhost:5000/tarefas/listar")
            .then((resposta) => resposta.json())
            .then((tarefas: Tarefa[]) => {
                console.table(tarefas);
                setTarefa(tarefas);
            })
    }

    return (

        <div>
            
            {tarefas.map((tarefa) => (

                <ul key={tarefa.tarefaId}>

                    <li> ID: {tarefa.tarefaId} </li>
                    <li> Titulo: {tarefa.titulo} </li>
                    <li> Descrição: {tarefa.descricao} </li>
                    <li> Status: {tarefa.status} </li>
                    <br />

                </ul>

            ))}

        </div>

    );

}

export default ListarTarefas;
