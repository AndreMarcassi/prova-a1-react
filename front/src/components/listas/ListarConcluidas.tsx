import { useEffect, useState } from "react";
import { Tarefa } from "../../models/Tarefa";

function ListarConcluidas() {
    const [tarefasConcuidas, setTarefasConcluidas] = useState<Tarefa[]>([]);

    useEffect(() => {
        carregarCategorias();
    }, []);

    function carregarCategorias() {
        // FETCH ou AXIOS
        fetch("http://localhost:5000/tarefas/concluidas")
            .then((resposta) => resposta.json())
            .then((tarefasConcuidas: Tarefa[]) => {
                console.table(tarefasConcuidas);
                setTarefasConcluidas(tarefasConcuidas);
            })
    }

    return (

        <div>
            
            {tarefasConcuidas.map((tarefa) => (

                <ul key={tarefa.tarefaId}>

                    <li> ID: {tarefa.tarefaId} </li>
                    <li> Titulo: {tarefa.titulo} </li>
                    <li> Descrição: {tarefa.descricao} </li>
                    <li> Status: {tarefa.status} </li>
                    <br />

                </ul>

            ))}

        </div>

    );

}

export default ListarConcluidas;
