import { useEffect, useState } from "react";
import { Tarefa } from "../models/Tarefa";


function CadastrarTarefa() {
    const [tarefas, setTarefas] = useState<Tarefa[]>([]);

    const [titulo, setTitulo] = useState("");
    const [descricao, setDescricao] = useState("");
    const [categoriaId, setCategoriaId] = useState("");
    const [status, setStatus] = useState("");

    useEffect(() => {
        carregartarefas(); 
    }, []);

  
  function carregartarefas() {
    //FETCH ou AXIOS
    fetch("http://localhost:5000/tarefas/listar")
        .then((resposta) => resposta.json()) 
        .then((tarefas: Tarefa[]) => {                   
            setTarefas(tarefas);
        }); 

  }

  function cadastrarTarefa(e: any) {

    const tarefa : Partial<Tarefa> = {
        titulo : titulo,
        descricao: descricao,
        categoriaId: categoriaId,
    };

    //FETCH ou AXIOS
    fetch("http://localhost:5000/tarefas/cadastrar", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(tarefa),
    })
        .then((resposta) => resposta.json())
        .then((tarefa : Tarefa) => {});

    e.preventDefault();

  }
  
  return (

    <form className="flex" style={{flexWrap: 'wrap'}} onSubmit={cadastrarTarefa}>
            
        <input className="form-input" type="text" placeholder="Digite um titulo para a tarefa" onChange={(e: any) => setTitulo(e.target.value)} required/>

        <input className="form-input" type="text" placeholder="Digite uma descrição para a tarefa" onChange={(e: any) => setDescricao(e.target.value)}/>

        <input className="form-input" type="text" placeholder="Digite o id de uma categoria para a tarefa" onChange={(e: any) => setCategoriaId(e.target.value)}/> 

        <input className="form-input" type="submit" value="Confirmar"></input>

    </form>
    
  );
}

export default CadastrarTarefa;